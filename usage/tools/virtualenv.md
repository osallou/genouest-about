# Virtualenv

To use it, first create a new virtualenv like this:

    . /local/env/envpython-3.6.3.sh
    virtualenv ~/my_new_env

This will create the directory ~/my_new_env. This directory will contain a
minimal copy of Python 3.6.3 (the one you sourced just before), without any
module installed in it, and completely isolated from the global 3.6.3 python
version installed by GenOuest. If you prefer to use a Python 2.7 version,
you can source 2.7.15 of Python if you prefer:

    . /local/env/envpython-2.7.15.sh
    virtualenv ~/my_new_env

To use this virtualenv, you need to activate it:

. ~/my_new_env/bin/activate
Once activate, your prompt will show that you activated a virtualenv:

    (my_new_env)[login@cl1n025 ~]$

You can then install all the Python modules you need in this virtualenv:

    pip install biopython
    pip install pyaml...

Now when you run python, you will be using the virtualenv’s Python version,
containing only the modules you installed in it.

Once you have finished working with the virtualenv, you can stop using it
and switch back to the normal environment like this:

    deactivate

You can create as many virtualenv as you want, each one being a directory
that you can safely remove when you don’t need it anymore.
