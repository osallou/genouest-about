# Slurm

It is forbidden to execute computations directly on the frontals
(genossh.genouest.org). You MUST first connect to a node (using srun) or
 submit a job to a node (using sbatch).

When you submit a job, it is dispatched on one of the computing nodes of the
cluster.

Those nodes have different characteristics (cpu, ram). We have servers from
128G up to 755G RAM on the nodes, with 8 to 40 cores each.
Launch the following command to display the list of available nodes
and their characteristics and load (memory in MB):

    sinfo -N -O nodelist,partition,freemem,cpusstate,memory

You can launch a shell on a computing node (equivalent to qrsh on SGE) using:

    srun --pty bash

You can submit a job with the “sbatch” command:

    sbatch my_script.sh

You can add submission options in the header of the script
using **SBATCH** directives:

    #!/bin/bash
    #SBATCH --job-name=test
    #SBATCH --chdir=workingdirectory
    #SBATCH --output=res.txt
    #SBATCH --ntasks=1
    #SBATCH --time=10:00
    #SBATCH --mem-per-cpu=100

You can submit jobs to a specific partition (the equivalent of SGE’s queues):

    sbatch -p genouest my_script.sh

By default, jobs are submitted to the main partition (“genouest”).
You only need to use this option for very specific cases.

You can monitor your jobs with the “squeue” command
(lists all the jobs by default, restrict to a specific user with
the -u option):

    squeue
    squeue -u username

Unlike SGE by default each job will be limited to 1 CPU and 6GB memory.
If you need more ressources, you need to add the following options to
srun or sbatch commands (or using SBATCH directives):

    sbatch --cpus-per-task=8 --mem=50G job_script.sh

In this example, we request 8 CPU and 50G memory on a node to execute the bash
script “job_script.sh”. Many options are available to finely tune the amount
of cpus and memory reserved for your job, have a look at the srun manual.
If at least 1 CPU and 6GB are not available on one node, you may have to wait
to be placed. You can use the same options when using srun.

Unlike SGE, these limits are strict, your job will not be allowed to use
more than was requested. If you use more than selected RAM, your process
will be killed.

To kill a job, simply execute:

    scancel XXX  (XXX is the job identifier)

To get more information on resource usage, you can:

* display more information for running jobs using squeue:

    squeue -l -o "%.18i %.9P %.100j %.8u %.2t %.10M %.6D %C %m %R"

* get information on a specific job:

    scontrol show job *job_id*

* check the maximum memory used by a running job:

    sstat -j *job_id*.batch --format="JobID,MaxRSS"

* check the cpu time and maximum memory used by a finished job:

    sacct -j *job_id* --format="JobID,CPUTime,MaxRSS,ReqMem"

Many other options to squeue, scontrol, sacct, and sstat are available,
you can consult their manual by running them with the –help option.

You can find a quick tutorial on Slurm on this [web site](https://slurm.schedmd.com/documentation.html).
