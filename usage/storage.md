# Storage

## Data storage

Once logged, you have access to three volumes, available on all computing nodes.

Your home directory (/home/genouest/*your-group*/*your-login*).
We have a total of around 100TB of storage capacity shared between all the home
directories, and each user have a quota of 100GB. You can check your disk usage
with the command “quota -s”.

A snapshot mechanism is available on this volumes, if you erased a file by
mistake, your can rescue it by looking into the ~/.snapshots directory.

A project directory (/groups/*your-group*) that you share with your team.
We have a total of around 200TB of storage capacity shared between all these
group directories.
Each project have a specific quota, and a single person in your team is
responsible to grant you access to this volume.
You can check your disk usage with the command:

    df -h /groups/<your-group>

A high performance storage space (/scratch/*your-login*).
Each user have a quota of 250GB. You can check your disk usage
with the command:

    du -sh /scratch/<your-login>

Quotas are intentionally restrictive, if you need them to be increased,
please contact support@genouest.org.

As a general rule, user should not write during the jobs in the /home
or /groups directory, nor do heavy read operations on these volume.
They are used to keep your data safe.
During jobs, one should use the /scratch directory.
This directory is hosted by a high performance system and designed for
temporary data. It supports heavy read and write operations.

Please note that none of your data is backed up. If you would like us to backup
your data for specific reasons, you can contact us and we will help you to
find a solution.

We strongly advise you to anticipate your storage needs: if you plan to
generate a big amount of data, please contact us before to check that we
have the possibility to host this data. It is preferable to anticipate this
when applying for grants that imply data generation and analysis.

Before generating data on the cluster, please do not forget to check the
remaining available space.
To do so, you may use the quota commands above, or use the df command for
global disk usage:

    df -h

## Snapshots

If you erase some files by mistake, you can recover the files by looking
in the *.snapshots* directory.
The snapshots are taken each hour and are kept for 5 weeks.
To access the snapshot files of your account just go to the *.snapshots* directory.

    cd .snapshots

There, you will see all the directories in which the files are stored.
The directories are easily recognizable by their name: hourly, daily, weekly.

**IMPORTANT**
The snapshot mechanism is only available on /home and /groups directory.

Please note that snapshots are *not* backups. They provide protection against
user error, but not against mechanical failure.

Please consider an external backup solution if your data is valuable.

## Cold storage

Cold storage is a storage where direct access to files is not directly
accessible via POSIX (read/write files).

We propose Openstack Swift storage (like Amazon S3). It can be used with
Swift compatible clients with Openstack API at
<https://genostack-api-swift.genouest.org/>:

* [herodote-file](https://github.com/osallou/herodote-file/releases)
* [python swift](https://pypi.org/project/python-swiftclient/)
or with a [web interface](https://genostack-data.genouest.org/login)

This means that files must be downloaded/uploaded when you need them, but
it also means they are accessible via HTTP from any location.
