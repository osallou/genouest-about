# Our tools

## Slurm

Our batch computing system is based on slurm.
From our bastion genossh.genouest.org, you can execute computing jobs or
connect interactivly to our nodes.

More [info and usage](tools/slurm.md)

## Software

### Installed

The list of available software is available [here](https://softwaremanager.genouest.org/#/)

You can also create your own environment with conda or virtualenv if the
software you want is not installed.

Most of our installed tools are also available through our
[Galaxy web portal](#Galaxy) for an easier use.

### Do it yourself

Some tools do not require specific rights, you can install them in your
own enviroment. To do so, the following tools are available:

#### conda

Conda is also available to install software on the cluster.

Conda environments behave much the same as python virtualenv,
but use conda packages instead of python modules.
A list of available conda packages is [here](https://anaconda.org/anaconda/repo).

Conda allows you to install the software you need in your own storage
volumes (/home, /groups or /omaha-beach).
The software needs to be available as Conda packages.

More [info and usage](tools/conda.md)

#### virtualenv

Several versions of Python are available on the cluster.
Each one comes with a specific set of modules preinstalled.
If you need to install a module, or to have a different module version,
you can use Virtualenv.
Virtualenvs are a way to create a custom Python environment, completely
isolated from the Python installation on the cluster. You can install all
the Python modules you want in this isolated environment.

More [info and usage](tools/virtualenv.md)

## Galaxy

[Galaxy](https://galaxyproject.org/) is a web portal for bioinformatics tools.

[Access our instance](https://galaxy.genouest.org/).

## Go-Docker

Dashboard (with API and CLI) to execute some Docker containers.
You can access to containers with your id or as root (with some restrictions).
 In container, you can ask for access to your home directory and shared
 storages or, with projects, group specific storage.

A job is basically a shell script to execute in selected container with its
cpu and memory requirements.

Dashboard also allows to submit jobs to slurm.

* [About](http://www.genouest.org/godocker/)
* [Dashboard](https://godocker.genouest.org)

## Openstack cloud

Private cloud to launch some virtual machines. You are the owner of the VM
(root). There is an expiration mechanism. If you do not extend the lifetime
of the VM (you receive a reminder email), VM is deleted.
Along VM you can attach additional disks or use a shared disk (manilla)
among them.

* [Openstack dashboard](https://genostack.genouest.org)
* [Usage](http://www.genouest.org/outils/genostack/getting-started.html)

## Databases

[my](https://my.genouest.org) self-service dashboard lets you create
on-demand MySQL databases. Choose a database name and database is
automatically created. An email containing credentials is sent once created.

Databases are hosted on our database server (genobdd)

## Cesgo

[CeSGO](https://cesgo.org) provides an integrated environment to help
scientists to work from project ideas to publication through data
production and management.

The CeSGO project is offered by GenOuest core facility and is funded
part of CPER by European funds, by state and by region Britain.

Among the different features you have access to a chat collaboration tool,
a project management tool (Kanboard using Kanban), a collaboration system
to share document/progress/info with your team (public or private), and a
file sharing tool based on Owncloud (like Dropbox).

All your data are hosted on Genouest resources (France) and remain private.

## Herodote

[Herodote](https://herodote.genouest.org/ui/login) is a "data to compute"
serverless software.

When you push data (new file or update a file) to the Openstack object storage
in a project (bucket), Herodote checks for hooks.
If a hook is defined for this data, then a job is automatically submitted.
The hook will download the file, execute the commands you defined and upload
the results back to the storage server.

All is about automation, users focus on data, not on jobs.
