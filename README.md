# Genouest core bioinformatics facility

Our facility provides development, trainings, expertise and resources for bioinformatics,
primarly for academic research.

Provided resources include, but not limited to, computing, biological
databanks, software and collaboration tools.

## Getting an account

To use our resources, you need an account. Only few web tools we host allow
anonymous usage.
To create an account, see [Genouest account](usage/create_an_account.md)

## Using our resources

We host computing resources to submit jobs, Galaxy instances, a private
cloud and much more...
[See them all](usage/tools.md).

If you have specific data storage requirements, we can discuss together
to increase your quotas, however we expect you come with a clear data
management plan (DMP).

Please contact us before submitting your research project proposal to ANR, etc.
if you plan to use our resources to plan your requirements and, eventually,
purchase needs.

Some questions about [storage](usage/storage.md)?

Interested by hosted [databanks](usage/biomaj.md)?

## Terms of use

To use Genouest services, you must comply with our [terms of use](policy/terms_of_use.md).

If a research article is published, we ask you to add a note that you used
our resources.

## RGPD and user data collection

All user information stored in Genouest information system are solely for
Genouest use and are not shared with anyone else.

Personal information (email, phone, address etc.) are used by administrators
only to contact user in the following cases:

* security issues
* service abuse
* communication (maintenance, etc.) via a mailing list. User can unsubscribe
  to the mailing list at any time.
* online services can make use of the user email address to send results on
  per user request

## Human resources

### Newcomers

We welcome newcomers, be it a permanent academic, a trainee or a developper
working on one of our wonderful projects.... In all cases, each one must
follow the same rules and will be trained to our quality and compute
environment.

* [Human resources information](human_resources/newcomers.md)
* [Quality management system](policy/iso9001.md)
* Like our users, you will need a [Genouest account](usage/create_an_account.md)
* [Our environement](usage/internal_tools.md)

## What we do

Managing a computing facility also means managing operating systems, networks,
user account and many other things. To do so, we develop internally lots of
software. And because we use lots of open source software, almost all our
developments are open source too. They are usually available at
[https://github.com/genouest](https://github.com/genouest).

We also have collaborations with other labs/entities such IFB cluster,
IFB cloud and of course the Elixir European project.

## Contact

Address:

    Plate-forme GenOuest IRISA-INRIA, Campus de Beaulieu 35042 Rennes cedex, France

Mail: support@genouest.org

Twitter: @GenOuest
