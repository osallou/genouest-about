# Newcomers

Hosted at [IRISA](https://www.irisa.fr/), an UMR (mixed research unit), we all
have different employers (INRIA, Universite Rennes 1, CNRS, etc.).
Salary, holidays, etc. are ruled by your employer.

As a newcomer, you will follow a 0.5 day presentation describing your
rights/obligations and local security rules with the local resource department
and the security officer.

In addition, you must follow the [IRISA internal regulations](https://intranet.irisa.fr/fr/intranet-fs/irisa/DocAdmin/ReglemementInterieurIRISA.pdf)

At your arrival, you will get issues assigned. You must complete them all!
