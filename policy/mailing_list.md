# Genouest mailing list

## About

Our mailing is a low rate list, and is used to send general information such as
maintenance and service issues.

Users are automatically subscribed at registration time.

## Unsubscribe

If, for any reason you want to unsubscribe of our mailing list, our mails
contain a link to do so.

You can also connect to the [Genouest account manager](https://my.genouest.org)
to manually unsubscribe or resubscribe at any time.
